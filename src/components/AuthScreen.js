import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { bool, func, object , string} from 'prop-types';
import {
  StyleSheet,
  View,
  Text,
  ImageBackground,
  Dimensions,
  LayoutAnimation,
  UIManager,
  KeyboardAvoidingView,
} from 'react-native';
// import { cacheFonts } from '../../helpers/AssetsCaching';
import { Input, Button, Icon } from 'react-native-elements';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const BG_IMAGE = require('../../assets/images/bg_screen4.jpg');

// Enable LayoutAnimation on Android
UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);

const TabSelector = ({ selected }) => {
  return (
    <View style={styles.selectorContainer}>
      <View style={selected && styles.selected} />
    </View>
  );
};

TabSelector.propTypes = {
  selected: bool.isRequired,
};

export class Auth extends Component {
  static navigationOptions = {
    header: null
  }

  static propTypes = {
      firstName: string,
      lastName: string,
      username: string,
      password: string,
      email: string,
      phone: string,
      userType: string,
      errorMessage: string,
      onSignUp: func,
      onFieldChange: func,
      // navigation: object,
  }
   
  static defaultProps = {
      // email: '',
      errorMessage: '',
      // password: '',
  }
  
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      fontLoaded: false,
      selectedCategory: 0,
      isLoading: false,
      isEmailValid: true,
      isPasswordValid: true,
      isConfirmationValid: true,
    };

    this.selectCategory = this.selectCategory.bind(this);
    // this.login = this.login.bind(this);
    // this.signUp = this.signUp.bind(this);
  }

  async componentDidMount() {
    // await cacheFonts({
    //   georgia: require('../../../assets/fonts/Georgia.ttf'),
    //   regular: require('../../../assets/fonts/Montserrat-Regular.ttf'),
    //   light: require('../../../assets/fonts/Montserrat-Light.ttf'),
    // });
    const { dispatch, isAuthenticated } = this.props;
    if (isAuthenticated) {
        dispatch(NavigationActions.navigate('HomeScreen'));
    }
    this.setState({ fontLoaded: true });
  }

  handleFieldChange = (field, value) => {
    this.props.onFieldChange(field, value);
  }
  
  handleLogin = () => {
      const { username, password, onLogin } = this.props;
      LayoutAnimation.easeInEaseOut();
      this.setState({ isLoading: true });
      onLogin(username, password);
  }
    
  handleSignUp = () => {
    console.log('handleSignUp- ');
    const { firstName, lastName, username, password, email, phone, userType, onSignUp } = this.props;
    LayoutAnimation.easeInEaseOut();
    if (this.validateFields()) {
      console.log('onSignUp true- ');
      this.setState({ isLoading: true });
      onSignUp(firstName, lastName, username, password, email, phone, userType);
    } else {
      this.setState({ isLoading: false });
    }
  }

  selectCategory(selectedCategory) {
    LayoutAnimation.easeInEaseOut();
    this.setState({
      selectedCategory,
      isLoading: false,
    });
  }

  validateFields = () => {
    const newState = {
      isEmailValid: true,
      isPasswordValid: true,
    }
    if (!this.validateEmail(this.props.email)) {
      newState.isEmailValid = false;
      this.setState(newState);
      return false;
    }
    if (this.props.password.length < 8 ) {
      newState.isPasswordValid = false;
      this.setState(newState);
      return false;
    }
    this.setState(newState);
    return true;
  }

  validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  render() {
    console.log('this.props- ', this.props);
    const {
      selectedCategory,
      isLoading,
      isEmailValid,
      isPasswordValid,
      isConfirmationValid,
      passwordConfirmation,
    } = this.state;
    const { firstName, lastName, username, password, email, phone, userType, errorMessage } = this.props;
    const isLoginPage = selectedCategory === 0;
    const isSignUpPage = selectedCategory === 1;
    return (
      <View style={styles.container}>
        <ImageBackground source={BG_IMAGE} style={styles.bgImage}>
          {this.state.fontLoaded ? (
            <View>
              <KeyboardAvoidingView
                contentContainerStyle={styles.loginContainer}
                behavior="position"
              >
                <View style={styles.titleContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.titleText}>OUR</Text>
                  </View>
                  <View style={{ marginTop: -10, marginLeft: 10 }}>
                    <Text style={styles.titleText}>SCHOOL</Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Button
                    disabled={isLoading}
                    type="clear"
                    activeOpacity={0.7}
                    onPress={() => this.selectCategory(0)}
                    containerStyle={{ flex: 1 }}
                    titleStyle={[
                      styles.categoryText,
                      isLoginPage && styles.selectedCategoryText,
                    ]}
                    title={'Login'}
                  />
                  <Button
                    disabled={isLoading}
                    type="clear"
                    activeOpacity={0.7}
                    onPress={() => this.selectCategory(1)}
                    containerStyle={{ flex: 1 }}
                    titleStyle={[
                      styles.categoryText,
                      isSignUpPage && styles.selectedCategoryText,
                    ]}
                    title={'Sign up'}
                  />
                </View>
                <View style={styles.rowSelector}>
                  <TabSelector selected={isLoginPage} />
                  <TabSelector selected={isSignUpPage} />
                </View>
                <View style={styles.formContainer}>
                  <Input
                    leftIcon={
                      <Icon
                        name="user-o"
                        type="font-awesome"
                        color="rgba(0, 0, 0, 0.38)"
                        size={25}
                        style={{ backgroundColor: 'transparent' }}
                      />
                    }
                    value={username}
                    keyboardAppearance="light"
                    autoFocus={false}
                    autoCapitalize="none"
                    autoCorrect={false}
                    keyboardType="email-address"
                    returnKeyType="next"
                    inputStyle={{ marginLeft: 10 }}
                    placeholder={'Username'}
                    containerStyle={{
                      borderBottomColor: 'rgba(0, 0, 0, 0.38)',
                    }}
                    ref={input => (this.usernameInput = input)}
                    onSubmitEditing={() => this.passwordInput.focus()}
                    onChangeText={value => this.handleFieldChange('username', value )}
                  />
                  <Input
                    leftIcon={
                      <Icon
                        name="lock"
                        type="simple-line-icon"
                        color="rgba(0, 0, 0, 0.38)"
                        size={25}
                        style={{ backgroundColor: 'transparent' }}
                      />
                    }
                    value={password}
                    keyboardAppearance="light"
                    autoCapitalize="none"
                    autoCorrect={false}
                    secureTextEntry={true}
                    returnKeyType={isSignUpPage ? 'next' : 'done'}
                    blurOnSubmit={true}
                    containerStyle={{
                      marginTop: 16,
                      borderBottomColor: 'rgba(0, 0, 0, 0.38)',
                    }}
                    inputStyle={{ marginLeft: 10 }}
                    placeholder={'Password'}
                    ref={input => (this.passwordInput = input)}
                    onSubmitEditing={() =>
                      isSignUpPage
                        ? this.confirmationInput.focus()
                        : this.handleLogin()
                    }
                    onChangeText={value => this.handleFieldChange('password', value )}
                    errorMessage={
                      isPasswordValid
                        ? null
                        : 'Please enter at least 8 characters'
                    }
                  />
                  {isSignUpPage && ([
                    <Input
                      icon={
                        <Icon
                          name="lock"
                          type="simple-line-icon"
                          color="rgba(0, 0, 0, 0.38)"
                          size={25}
                          style={{ backgroundColor: 'transparent' }}
                        />
                      }
                      key="confirmation"
                      value={passwordConfirmation}
                      secureTextEntry={true}
                      keyboardAppearance="light"
                      autoCapitalize="none"
                      autoCorrect={false}
                      keyboardType="default"
                      returnKeyType={'done'}
                      blurOnSubmit={true}
                      containerStyle={{
                        marginTop: 16,
                        borderBottomColor: 'rgba(0, 0, 0, 0.38)',
                      }}
                      inputStyle={{ marginLeft: 10 }}
                      placeholder={'Confirm password'}
                      ref={input => (this.confirmationInput = input)}
                      onSubmitEditing={() => this.firstNameInput.focus()}
                      onChangeText={passwordConfirmation =>
                        this.setState({ passwordConfirmation })
                      }
                      errorMessage={
                        isConfirmationValid
                          ? null
                          : 'Please enter the same password'
                      }
                    />,
                    <Input
                      key="firstName"
                      value={firstName}
                      keyboardAppearance="light"
                      autoFocus={false}
                      autoCapitalize="none"
                      autoCorrect={false}
                      keyboardType="default"
                      returnKeyType="next"
                      inputStyle={{ marginLeft: 10 }}
                      placeholder={'First Name'}
                      containerStyle={{
                        borderBottomColor: 'rgba(0, 0, 0, 0.38)',
                      }}
                      ref={input => (this.firstNameInput = input)}
                      onSubmitEditing={() => this.lastNameInput.focus()}
                      onChangeText={value => this.handleFieldChange('firstName', value )}
                    />,
                    <Input
                      key="lastName"
                      value={lastName}
                      keyboardAppearance="light"
                      autoFocus={false}
                      autoCapitalize="none"
                      autoCorrect={false}
                      keyboardType="default"
                      returnKeyType="next"
                      inputStyle={{ marginLeft: 10 }}
                      placeholder={'Last Name'}
                      containerStyle={{
                        borderBottomColor: 'rgba(0, 0, 0, 0.38)',
                      }}
                      ref={input => (this.lastNameInput = input)}
                      onSubmitEditing={() => this.emailInput.focus()}
                      onChangeText={value => this.handleFieldChange('lastName', value )}
                    />,
                    <Input
                      leftIcon={
                        <Icon
                          name="envelope-o"
                          type="font-awesome"
                          color="rgba(0, 0, 0, 0.38)"
                          size={25}
                          style={{ backgroundColor: 'transparent' }}
                        />
                      }
                      key="email"
                      value={email}
                      keyboardAppearance="light"
                      autoFocus={false}
                      autoCapitalize="none"
                      autoCorrect={false}
                      keyboardType="email-address"
                      returnKeyType="next"
                      inputStyle={{ marginLeft: 10 }}
                      placeholder={'asd@abc.com'}
                      containerStyle={{
                        borderBottomColor: 'rgba(0, 0, 0, 0.38)',
                      }}
                      ref={input => (this.emailInput = input)}
                      onSubmitEditing={() => this.phoneInput.focus()}
                      onChangeText={value => this.handleFieldChange('email', value )}
                      errorMessage={
                        isEmailValid ? null : 'Please enter a valid email address'
                      }
                    />,
                    <Input
                      leftIcon={
                        <Icon
                          name="phone"
                          type="font-awesome"
                          color="rgba(0, 0, 0, 0.38)"
                          size={25}
                          style={{ backgroundColor: 'transparent' }}
                        />
                      }
                      key="phone"
                      value={phone}
                      keyboardAppearance="light"
                      autoFocus={false}
                      autoCapitalize="none"
                      autoCorrect={false}
                      keyboardType="name-phone-pad"
                      returnKeyType="next"
                      inputStyle={{ marginLeft: 10 }}
                      placeholder={'Phone/Mobile'}
                      containerStyle={{
                        borderBottomColor: 'rgba(0, 0, 0, 0.38)',
                      }}
                      ref={input => (this.phoneInput = input)}
                      onSubmitEditing={this.handleSignUp}
                      onChangeText={value => this.handleFieldChange('phone', value )}
                    />
                    ])}
                  <Button
                    buttonStyle={styles.loginButton}
                    containerStyle={{ marginTop: 32, flex: 0 }}
                    activeOpacity={0.8}
                    title={isLoginPage ? 'LOGIN' : 'SIGN UP'}
                    onPress={isLoginPage ? this.handleLogin : this.handleSignUp}
                    titleStyle={styles.loginTextButton}
                    loading={isLoading}
                    disabled={isLoading}
                  />
                </View>
              </KeyboardAvoidingView>
              <View style={styles.helpContainer}>
                <Button
                  title={'Need help ?'}
                  titleStyle={{ color: 'white' }}
                  buttonStyle={{ backgroundColor: 'transparent' }}
                  underlayColor="transparent"
                  onPress={() => console.log('Account created')}
                />
              </View>
            </View>
          ) : (
            <Text>Loading...</Text>
          )}
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  rowSelector: {
    height: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  selectorContainer: {
    flex: 1,
    alignItems: 'center',
  },
  selected: {
    position: 'absolute',
    borderRadius: 50,
    height: 0,
    width: 0,
    top: -5,
    borderRightWidth: 70,
    borderBottomWidth: 70,
    borderColor: 'white',
    backgroundColor: 'white',
  },
  loginContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  loginTextButton: {
    fontSize: 16,
    color: 'white',
    fontWeight: 'bold',
  },
  loginButton: {
    backgroundColor: 'rgba(232, 147, 142, 1)',
    borderRadius: 10,
    height: 50,
    width: 200,
  },
  titleContainer: {
    height: 150,
    backgroundColor: 'transparent',
    justifyContent: 'center',
  },
  formContainer: {
    backgroundColor: 'white',
    width: SCREEN_WIDTH - 30,
    borderRadius: 10,
    paddingTop: 32,
    paddingBottom: 32,
    alignItems: 'center',
  },
  loginText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white',
  },
  bgImage: {
    flex: 1,
    top: 0,
    left: 0,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
    justifyContent: 'center',
    alignItems: 'center',
  },
  categoryText: {
    textAlign: 'center',
    color: 'white',
    fontSize: 24,
    // fontFamily: 'light',
    backgroundColor: 'transparent',
    opacity: 0.54,
  },
  selectedCategoryText: {
    opacity: 1,
  },
  titleText: {
    color: 'white',
    fontSize: 30,
    // fontFamily: 'regular',
  },
  helpContainer: {
    height: 64,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
