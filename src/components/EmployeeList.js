import React from 'react';
import { Card, ListItem } from 'react-native-elements';

export const EmployeeList = (imageUrl) => {
    const users = [
        {
          name: 'Amy Farha',
          avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
          role: 'Vice President'
        },
        {
          name: 'Chris Jackson',
          avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
          role: 'Vice Chairman'
        },
    ]
    return (
        <Card containerStyle={{padding: 0}} >
        {
            users.map((u, i) => {
            return (
                <ListItem
                    leftAvatar={{
                        source: { uri: u.avatar_url },
                        showEditButton: true,
                    }}
                    title={u.name}
                    subtitle={u.role}
                    chevron
                />
            );
            })
        }
        {/* 
                <ListItem
                    key={i}
                    roundAvatar
                    title={u.name}
                    avatar={{uri: u.avatar_url}}
                    subtitle={u.role}
                />
        */}
        </Card>
    );
};
