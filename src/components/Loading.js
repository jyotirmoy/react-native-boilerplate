import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { View, Text, ActivityIndicator, StyleSheet } from 'react-native';
import { NavigationActions } from 'react-navigation';

class Loading extends React.Component {
    componentDidMount() {
      const { dispatch, isAuthenticated } = this.props;
      debugger;
      dispatch(NavigationActions.navigate(isAuthenticated ? 'HomeScreen' : 'AuthScreen'));
    }

    render() {
      console.log('loading props- ', this.props);
      return (
          <View style={styles.container}>
              <Text style={{color:'#e93766', fontSize: 40}}>Loading</Text>
              <ActivityIndicator color='#e93766' size="large" />
          </View>
      )
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});

const mapStateToProps = (state) => {
  const { isAuthenticated } = state.auth;
  return {
    isAuthenticated,
  }
};

const mapDispatchToProps = dispatch => bindActionCreators(
  {
      dispatch,
  },
  dispatch,
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Loading);
