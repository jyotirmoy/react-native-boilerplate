import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Auth } from './AuthScreen';
import { actionCreators } from '../ducks/AuthDucks';

const mapStateToProps = (state) => {
    const { firstName, lastName, username, password, email, phone, userType, errorMessage, loading, isAuthenticated } = state.auth;
    return {
        firstName,
        lastName,
        username,
        password,
        email,
        phone,
        userType,
        errorMessage,
        loading,
        isAuthenticated,
    }
};

const mapDispatchToProps = dispatch => bindActionCreators(
    {
        onLogin: actionCreators.onLogin,
        onSignUp: actionCreators.onSignUp,
        onFieldChange: actionCreators.onFieldChange,
        dispatch,
    },
    dispatch,
);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Auth);
