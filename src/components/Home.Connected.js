import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Home from './Home';
import { actionCreators } from '../ducks/AuthDucks';

const mapStateToProps = (state) => {
    const { username, isAuthenticated, errorMessage } = state.auth;
    return {
        username,
        isAuthenticated,
        errorMessage,
    }
};

const mapDispatchToProps = dispatch => bindActionCreators(
    {
        onSignout: actionCreators.onSignout,
        onFieldChange: actionCreators.onFieldChange,
        dispatch,
    },
    dispatch,
);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);
