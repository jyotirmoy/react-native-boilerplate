// Home.js
import React, { Component } from 'react';
import {Button, Platform, StyleSheet, Text, View} from 'react-native';
import { NavigationActions } from 'react-navigation';

import {EmployeeList} from './EmployeeList';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

class Home extends Component {

    static navigationOptions = {
        title: `School`,
    };

    state = { currentUser: null }

    componentDidMount() {
        const { currentUser } = {}; // firebase.auth();
        this.setState({ currentUser })
    }

    signOut = () => {
        this.props.onSignout();
    }

    render() {
        const { currentUser } = this.state;
        return (
        <View>
            <Text>This is the home screen</Text>
            <View>
                <Text style={styles.welcome}>
                    Hi {currentUser && currentUser.email}!
                </Text>
                <Text style={styles.instructions}>To get started, edit Home.js or import other components into Home.js</Text>
                <Text style={styles.instructions}>{instructions}</Text>

                <Button title="Sign Out" color="#e93766" onPress={this.signOut} />
                <EmployeeList />
                <Text
                    onPress={() => this.props.dispatch(NavigationActions.navigate({ routeName: 'EmployeeDetailsScreen' }))}
                    style={{color:'#e93766', fontSize: 18}}
                >
                    Employee Details
                </Text>
            </View>
        </View>
        )
    }
}

export default Home;
// export default connect(state => ({ count: state.count }))(Home);

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
      height: 800,
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
  });
