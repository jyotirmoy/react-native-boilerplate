/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import { createStackNavigator, NavigationActions } from 'react-navigation';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import {
  createReduxContainer,
  createReactNavigationReduxMiddleware,
  createNavigationReducer,
} from 'react-navigation-redux-helpers';
import { Provider, connect } from 'react-redux';
import thunk from 'redux-thunk';
import React, {Component} from 'react';
import Parse from 'parse/react-native';
import { AsyncStorage, BackHandler } from 'react-native';

import { ThemeProvider } from 'react-native-elements';

import Settings from './components/Settings';
import Home from './components/Home.Connected';
import Loading from './components/Loading';
import AuthScreen from './components/Auth.Connected';
import EmployeeDetailsScreen from './components/EmployeeDetails';
import ListsScreen from './components/Lists';

// const MainNavigator = DrawerNavigator({
//   BookCreate: { screen: BookCreate },
//   BookList: { screen: BookList },
//   BookEdit: { screen: BookEdit }
// });

// const AppNavigator = StackNavigator({
//   Login: { screen: Login },
//   Main: { screen: MainNavigator }
// });

const AppNavigator = createStackNavigator(
  {
    LoadingScreen: { screen: Loading },
    HomeScreen: { screen: Home },
    SettingScreen: { screen: Settings },
    AuthScreen: { screen: AuthScreen },
    EmployeeDetailsScreen: { screen: EmployeeDetailsScreen },
    ListsScreen: { screen: ListsScreen },
    // Main: { screen: MainNavigator },
  }, {
    initialRouteName: 'SettingScreen'
  }
)

import AuthReducer from './ducks/AuthDucks';
import EmployeeReducer from './ducks/EmployeeDucks';

const navReducer = createNavigationReducer(AppNavigator);
const appReducer = combineReducers({
  nav: navReducer,
  auth: AuthReducer,
  employees: EmployeeReducer,
});

const middleware = createReactNavigationReduxMiddleware(
  state => state.nav,
);

const App = createReduxContainer(AppNavigator);
const mapStateToProps = (state) => ({
  state: state.nav,
});
const AppWithNavigationState = connect(mapStateToProps)(App);

const store = createStore(
  appReducer,
  applyMiddleware(middleware, thunk),
);

export default class Root extends Component {
  componentWillMount() {
    Parse.initialize("uXp5sdw61Jv6ZzqJoCPlqr4YNw6idJHAI9NLGxxi", "6ebpUBYlvk4Ky7ho3StVulS7u3s5PVMGyniPXKuu");
    Parse.serverURL = 'https://parseapi.back4app.com/';
    Parse.setAsyncStorage(AsyncStorage);
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
  }

  onBackPress = () => {
    const { dispatch, nav } = this.props;
    if (nav.index === 0) {
      return false;
    }

    dispatch(NavigationActions.back());
    return true;
  };

  render() {
    return (
      <Provider store={store}>
        <ThemeProvider>
          <AppWithNavigationState />
        </ThemeProvider>
      </Provider>
    );
  }
}
